import React from 'react';
import RowComponent from './RowComponent';
export default class FooterComponent extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <tfoot>
                <RowComponent data={this.props.data} />
            </tfoot>
        )
    }
}
