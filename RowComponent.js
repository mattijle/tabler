import uuid from 'uuid';
import React from 'react';
import CellComponent from './CellComponent';
export default class RowComponent extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <tr>
                {this.props.data.map((d)=>{
                    return <CellComponent data={d} key={uuid.v4()}/>                          
                })}
            </tr>
        )
    }
}
