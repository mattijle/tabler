Tabler
===

Simple [React](https://facebook.github.io/react/) component for displaying tabular data.

Pass Array of Arrays and field names as props.
```
    import React from 'react';
    import TableComponent from './TableComponent';
    var arr = [
        [1,2,3,4],
        [5,6,7,8]
    ];
    React.render(<TableComponent data={arr} keys={['val1,'val2',val3','val4']} />,document.body);
```