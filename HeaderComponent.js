import React from 'react';
import RowComponent from './RowComponent';
export default class HeaderComponent extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <thead>
                <RowComponent data={this.props.data} />
            </thead>
        )
    }
}
