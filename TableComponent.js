import uuid from 'uuid';
import React from 'react';
import HeaderComponent from './HeaderComponent';
import FooterComponent from './FooterComponent';
import RowComponent from './RowComponent';
export default class TableComponent extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <table>
                <HeaderComponent data={this.props.keys}/>
                <tbody>
                {this.props.data.map((ent)=>{
                    return <RowComponent data={ent} key={uuid.v4()} />
                })}
                </tbody>
                <FooterComponent data={this.props.keys}/>
            </table>
        )
    }
};